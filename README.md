# Baokim gateway for Omnipay

## Installation

Omnipay Baokim is installed via [Composer](http://getcomposer.org/):

```
composer require ngocnh/omnipay-baokim:~1.0
```

## Example

```php
$baokim = Omnipay::create('BaoKim_Express');
$baokim->setBusiness('dev.baokim@bk.vn');
$baokim->setMerchant('647');
$baokim->setWebsitePassword('ae543c080ad91c23');
$baokim->setApiUsername('merchant');
$baokim->setApiPassword('1234');
$baokim->setApiSignature('private_key');

$baokim->setTestMode(true);

$params = [
	'cancelUrl'   => 'http://localhost/cancel',
	'returnUrl'   => 'http://localhost/success',
	'amount'      => '10.00',
	'order_id'    => '01001-rb',
	'description' => '#01001-rb',
	'currency'    => 'USD'
];

$response = $baokim->purchase($params)->send();

if ($response->isSuccessful()) {
	// payment was successful: update database
	print_r($response);
} elseif ($response->isRedirect()) {

	// redirect to offsite payment gateway
	$response->redirect();
} else {
	// payment failed: display message to customer
	echo $response->getMessage();
}
```